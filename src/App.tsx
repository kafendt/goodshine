import { Route, Switch } from "react-router-dom";
import React from "react";

import Home from "./pages/customer/Home";
import Login from "./pages/Login";
import PrivateRoute, {
  LoggedInUserContext
} from "./components/login/PrivateRoute";
import Map from "./pages/customer/Map";
import ShopDetails from "./pages/customer/ShopDetails";
import Checkout from "./pages/customer/Checkout";
import VoucherList from "./pages/customer/VoucherList";
import VoucherDetails from "./pages/customer/VoucherDetails";
import MerchantDashboard from "./pages/merchant/MerchantDashboard";
import Register from "./pages/merchant/Register";
import VerifyVoucher from "./pages/merchant/VerifyVoucher";
import VerificationList from "./pages/admin/VerificationList";
import { useShops } from "./db/shopStore";
import { Fetcher } from "./tools/Fetcher";

const App: React.FC = () => {
  return (
    <div>
      <Switch>
        <Route exact path={"/login"}>
          <main>
            <div />
            <Login />
          </main>
        </Route>

        <PrivateRoute path={"/customer"} role="customer">
          <LoggedInUserContext.Consumer>
            {user => (
              <Switch>
                <Route exact path={"/customer/"}>
                  <Fetcher hooks={{ shops: useShops }}>
                    <Home user={user} />
                  </Fetcher>
                </Route>
                <Route exact path={"/customer/map"}>
                  <Map user={user} />
                </Route>
                <Route path={"/customer/shop/:id"}>
                  <ShopDetails user={user} />
                </Route>
                <Route exact path={"/customer/checkout"}>
                  <Checkout user={user} />
                </Route>
                <Route path={"/customer/voucher/:id"}>
                  <VoucherDetails user={user} />
                </Route>
                <Route exact path={"/customer/vouchers"}>
                  <VoucherList user={user} />
                </Route>
              </Switch>
            )}
          </LoggedInUserContext.Consumer>
        </PrivateRoute>
        <PrivateRoute path={"/merchant"} role="merchant">
          <LoggedInUserContext.Consumer>
            {user => (
              <Switch>
                <Route path={"/merchant/register"}>
                  <Register user={user} />
                </Route>
                <Route path={"/merchant/verify/:verificationId"}>
                  <VerifyVoucher user={user} />
                </Route>
                <Route path={"/merchant/"}>
                  <MerchantDashboard user={user} />
                </Route>
              </Switch>
            )}
          </LoggedInUserContext.Consumer>
        </PrivateRoute>
        <PrivateRoute path={"/admin"} role="admin">
          <LoggedInUserContext.Consumer>
            {user => (
              <Switch>
                <Route exact path={"/admin/verification"}>
                  <VerificationList user={user} />
                </Route>
              </Switch>
            )}
          </LoggedInUserContext.Consumer>
        </PrivateRoute>
      </Switch>
    </div>
  );
};

export default App;
