import React from "react";
import { DataTuple } from "../db/types";

export interface FetcherProps {
  readonly hooks: { [name: string]: () => DataTuple<any> };
  children: React.ReactNode;
}

export function Fetcher(props: FetcherProps) {
  const fetchedDataTuples = Object.entries(props.hooks).map(([name, hook]) => {
    return [name, hook()];
  });

  // Handle loading
  const isUndefined = (element: (string | DataTuple<any>)[]) => !!element[1][1];
  if (fetchedDataTuples.some(isUndefined)) {
    return <h1>Loading ...</h1>;
  }

  // Handle error
  for (const [, dataTuple] of fetchedDataTuples) {
    if (dataTuple[2]) {
      return <h1>Error...</h1>;
    }
  }

  // Pass fetched data only
  const relevantProps = fetchedDataTuples.map(([name, dataTuple]) => [
    name,
    dataTuple[0]
  ]);
  const childProps = Object.fromEntries(relevantProps);

  const children = React.Children.map(props.children, child => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child as React.ReactElement<any>, {
        ...childProps
      });
    }
  });

  return <>{children}</>;
}
