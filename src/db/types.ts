export type DataTuple<T> = [T | undefined, boolean, Error | undefined];
