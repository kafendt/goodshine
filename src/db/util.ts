export function stripUndefined(v: Object): Object {
  return Object.fromEntries(
    Object.entries(v).filter(([, value]) => value !== undefined)
  );
}
