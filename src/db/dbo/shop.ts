import firebase from "firebase";

export interface ShopDBO {
  readonly id: string;
  readonly ownerId: string;
  readonly address: string;
  readonly geolocation: firebase.firestore.GeoPoint | undefined;
  readonly discountPercent: number;
  readonly name: string;
  readonly description: string;
  readonly verificationDocumentId: string;
  readonly state: string;
}

export interface VerificationDocumentDBO {
  readonly id: string;
  readonly ownerId: string;
  readonly blob: firebase.firestore.Blob;
  readonly fileType: string;
}
