import { Role } from "../../model/user";

export interface UserProfileDBO {
  readonly id: string;
  readonly email: string | undefined;
  readonly displayName: string | undefined;
  readonly avatarUrl: string | undefined;
  readonly role: Role;
}
