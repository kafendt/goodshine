export interface VoucherDBO {
  readonly id: string;
  readonly token: string;
  readonly shopId: string;
  readonly customerId: string;
  readonly amount: number;
  readonly creationDate: string;
  readonly log: AuditLogEntryDBO[];
}

export interface AuditLogEntryDBO {
  readonly state: string;
  readonly date: string;
}
