import { Role, User } from "../model/user";
import { DataTuple } from "./types";
import firebase from "firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { useCollection, useDocumentData } from "react-firebase-hooks/firestore";
import { UserProfileDBO } from "./dbo/user";

/**
 * Get a single User. Currently User is just a view on the firebase specific UserInfo,
 * this should in the future also take care to merge any profile data that we
 * persist additionally.
 */
export function useUserLoading(): DataTuple<User> {
  const auth = firebase.auth();
  const [userInfo, loading, error] = useAuthState(auth);
  const dataTuple = useUserProfile(userInfo?.uid ?? "dummy");

  if (error) {
    return [undefined, loading, new Error(`[${error.code}] ${error.message}`)];
  }

  if (loading || !userInfo) {
    return [undefined, loading, undefined];
  }

  return dataTuple;
}

/**
 * Get the currently logged in user, or throw.
 *
 * Only use inside [PrivateRoute]
 */
export function useLoggedInUser(): User {
  const [user] = useUserLoading();
  if (!user) {
    throw new Error("Invalid State: No user in cache!");
  }

  return user;
}

const USERS_COLLECTION = "users";

/**
 * Get a user profile by id
 * @param id
 */
export function useUserProfile(id: string): DataTuple<User> {
  const ref = firebase.firestore().doc(`${USERS_COLLECTION}/${id}`);
  const [snapshot, loading, error] = useDocumentData(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !snapshot) {
    return [undefined, loading, undefined];
  }

  return [User.fromDBO(snapshot as UserProfileDBO), false, undefined];
}

/**
 * Get all user profiles
 *
 * TODO:
 *  - must filter by e.g. company / project in future
 */
export function useUserProfiles(): DataTuple<{ [id: string]: User }> {
  const ref = firebase.firestore().collection(USERS_COLLECTION);

  const [snapshot, loading, error] = useCollection(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !snapshot) {
    return [undefined, loading, undefined];
  }

  const result: { [id: string]: User } = {};
  for (const doc of snapshot.docs) {
    result[doc.id] = User.fromDBO({
      ...(doc.data() as UserProfileDBO),
      id: doc.id
    });
  }

  return [result, false, undefined];
}

export async function registerUser(
  authData: firebase.auth.UserCredential,
  role: Role
): Promise<User> {
  if (!authData.user) {
    throw new Error("login failed");
  }

  const { displayName, email, uid } = authData.user;

  const ref = firebase
    .firestore()
    .doc(`${USERS_COLLECTION}/${authData.user.uid}`);

  if (!email || !displayName) {
    throw new Error("No user data");
  }

  const existingUser = await ref.get();
  if (existingUser.data()) {
    return User.fromDBO(existingUser.data() as UserProfileDBO);
  }

  const dbo: UserProfileDBO = {
    id: uid,
    avatarUrl: "",
    displayName,
    email,
    role
  };

  await ref.set(dbo);

  return User.fromDBO(dbo);
}
