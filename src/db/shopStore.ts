import { Shop, VerificationDocument, VerificationState } from "../model/shop";
import firebase from "firebase";
import { ShopDBO, VerificationDocumentDBO } from "./dbo/shop";
import { DataTuple } from "./types";
import {
  useCollectionData,
  useDocumentData
} from "react-firebase-hooks/firestore";
import { stripUndefined } from "./util";
import uuid from "uuid/v4";

const SHOP_COLLECTION = "shops";
const DOCUMENT_COLLECTION = "documents";

/**
 * Get a single shop
 * @param id
 */
export function useShop(id: string): DataTuple<Shop> {
  const ref = firebase.firestore().doc(`${SHOP_COLLECTION}/${id}`);
  const [dbo, loading, error] = useDocumentData<ShopDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !dbo) {
    return [undefined, loading, undefined];
  }

  return [Shop.fromDBO(dbo), false, undefined];
}

/**
 * Get a List of Shops.
 */
export function useShops(
  acceptedOnly: boolean = true
): DataTuple<{ [id: string]: Shop }> {
  const ref = acceptedOnly
    ? firebase
        .firestore()
        .collection(SHOP_COLLECTION)
        .where("state", "==", "accepted")
    : firebase.firestore().collection(SHOP_COLLECTION);

  const [documents, loading, error] = useCollectionData<ShopDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !documents) {
    return [undefined, loading, undefined];
  }

  const result: { [id: string]: Shop } = {};
  for (const dbo of documents.values()) {
    result[dbo.id] = Shop.fromDBO(dbo);
  }

  return [result, false, undefined];
}

export function uploadDocument(document: VerificationDocument): Promise<void> {
  const ref = firebase.firestore().doc(`${DOCUMENT_COLLECTION}/${document.id}`);
  return ref.set(document.serialize());
}

export function useDocument(id: string): DataTuple<VerificationDocument> {
  const ref = firebase.firestore().doc(`${DOCUMENT_COLLECTION}/${id}`);
  const [dbo, loading, error] = useDocumentData<VerificationDocumentDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !dbo) {
    return [undefined, loading, undefined];
  }

  return [VerificationDocument.fromDBO(dbo), false, undefined];
}

export function saveShop(shop: Shop): Promise<void> {
  const ref = firebase.firestore().doc(`${SHOP_COLLECTION}/${shop.id}`);
  return ref.set(stripUndefined(shop.serialize()));
}
