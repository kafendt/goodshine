import { Voucher } from "../model/voucher";
import firebase from "firebase";
import { stripUndefined } from "./util";
import { DataTuple } from "./types";
import {
  useCollectionData,
  useDocumentData
} from "react-firebase-hooks/firestore";
import { VoucherDBO } from "./dbo/voucher";

const VOUCHERS_COLLECTION = "vouchers";

/**
 * Get a single voucher.
 *
 * @param id
 */
export function useVoucher(id: string): DataTuple<Voucher> {
  const ref = firebase.firestore().doc(`${VOUCHERS_COLLECTION}/${id}`);
  const [dbo, loading, error] = useDocumentData<VoucherDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !dbo) {
    return [undefined, loading, undefined];
  }

  return [Voucher.fromDBO(dbo), false, undefined];
}

/**
 * Get a single voucher from token instead of id.
 * Make sure to always query by id when possible.
 *
 * @param token
 */
export function useVoucherFromToken(token: string): DataTuple<Voucher> {
  const ref = firebase
    .firestore()
    .collection(VOUCHERS_COLLECTION)
    .where("token", "==", token);
  const [vouchers, loading, error] = useCollectionData<VoucherDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !vouchers || !vouchers.length) {
    return [undefined, loading, undefined];
  }

  console.log(vouchers);
  return [Voucher.fromDBO(vouchers.pop()!!), false, undefined];
}

/**
 * Get a list of vouchers.
 */
export function useVouchers(): DataTuple<{ [id: string]: Voucher }> {
  const ref = firebase.firestore().collection(VOUCHERS_COLLECTION);

  const [vouchers, loading, error] = useCollectionData<VoucherDBO>(ref);

  if (error) {
    return [undefined, loading, error];
  }

  if (loading || !vouchers) {
    return [undefined, loading, undefined];
  }

  const result: { [id: string]: Voucher } = {};
  for (const dbo of vouchers.values()) {
    result[dbo.id] = Voucher.fromDBO(dbo);
  }

  return [result, false, undefined];
}

export function upsertVoucher(voucher: Voucher): Promise<void> {
  const ref = firebase.firestore().doc(`${VOUCHERS_COLLECTION}/${voucher.id}`);
  return ref.set(stripUndefined(voucher.serialize()));
}
