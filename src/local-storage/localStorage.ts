import React from "react";

export enum LocalStorageKey {
  SeasonId = "SeasonId"
}

export function useStateWithLocalStorage(
  localStorageKey: LocalStorageKey
): [string | undefined, (key: string) => void] {
  const [value, setValue] = React.useState(
    localStorage.getItem(localStorageKey.toString()) || undefined
  );

  React.useEffect(() => {
    if (value !== undefined && value !== null) {
      localStorage.setItem(localStorageKey.toString(), value);
    }
  }, [localStorageKey, value]);

  return [value, setValue];
}
