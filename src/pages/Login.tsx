import { Redirect, useHistory } from "react-router";
import * as React from "react";
import * as AuthTypes from "@firebase/auth-types";
import { registerUser, useUserLoading } from "../db/userStore";
import firebase from "firebase";
import { Role, User } from "../model/user";
const GoogleAuthProvider = firebase.auth.GoogleAuthProvider;

const googleAuthProvider = new GoogleAuthProvider();
googleAuthProvider.addScope("profile");
googleAuthProvider.addScope("email");

const Login: React.FC = () => {
  const auth = firebase.auth();
  const [user, loading, error] = useUserLoading();
  const history = useHistory();

  const redirect = (u: User) => {
    if (u.role === "merchant") {
      history.replace("/merchant/register");
      return;
    }

    if (u.role === "admin") {
      history.replace("/admin/verification");
      return;
    }

    if (u.role === "customer") {
      history.replace("/customer/");
      return;
    }
  };

  const loginWithGoogle = async (registerAs: Role) => {
    const firebaseUser: Promise<AuthTypes.UserCredential> = auth.signInWithPopup(
      googleAuthProvider
    );
    firebaseUser
      .then(async userCredential => {
        const user = await registerUser(userCredential, registerAs);
        redirect(user);
      })
      .catch(function(err) {
        // TODO(FP): error handling
        console.error("Login failed: ", err);
      });
  };

  if (error) {
    console.error("Error: ", error);
    // TODO(FP): create error page
    return <Redirect to={"/error"} />;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!user) {
    return (
      <div>
        <button onClick={() => loginWithGoogle("merchant")}>Händler</button>
        <button onClick={() => loginWithGoogle("customer")}>Supporter</button>
      </div>
    );
  }

  redirect(user);
  return null;
};

export default Login;
