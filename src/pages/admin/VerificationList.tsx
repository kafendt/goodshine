import * as React from "react";
import { SessionProps } from "../../components/login/PrivateRoute";
import { useShops } from "../../db/shopStore";
import { Redirect } from "react-router";
import AdminShopList from "../../components/admin/AdminShopList";

const VerificationList: React.FC<SessionProps> = ({ user }) => {
  const [shops, loading, error] = useShops(false);

  return (
    <main>
      <h2>Verifikation</h2>
      {loading && <span>Loading...</span>}
      {error && <Redirect to="/error" />}
      {shops && <AdminShopList shops={shops} />}
    </main>
  );
};

export default VerificationList;
