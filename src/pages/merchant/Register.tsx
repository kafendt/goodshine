import * as React from "react";
import { SessionProps } from "../../components/login/PrivateRoute";
import MerchantRegistrationForm, {
  ShopRegisterFormResult
} from "../../components/merchant/MerchantRegistrationForm";
import { useState } from "react";
import uuid from "uuid/v4";
import { Shop, VerificationDocument } from "../../model/shop";
import { saveShop, uploadDocument } from "../../db/shopStore";
import { useHistory } from "react-router";

const Register: React.FC<SessionProps> = ({ user }) => {
  const [errorMessage, setErrorMessage] = useState<string | undefined>();
  const [locked, setLocked] = useState<boolean>(false);
  const history = useHistory();

  const createStore = async (s: ShopRegisterFormResult) => {
    // TODO resolve geolocation
    const geolocation = undefined;

    try {
      setLocked(true);

      const document = new VerificationDocument(
        uuid(),
        user.id,
        s.verificationDocument,
        s.fileType
      );
      await uploadDocument(document);

      const shop = new Shop(
        uuid(),
        user.id,
        s.address,
        geolocation,
        s.discount,
        s.name,
        s.description,
        document.id,
        "pending"
      );

      await saveShop(shop);
      history.push("/merchant/");
    } catch (e) {
      setLocked(false);
      console.error("Could not create shop: ", e);
      setErrorMessage(
        "Sorry, Ihr Geschäft kann derzeit nicht gespeichert werden. Versuchen Sie es später erneut."
      );
    }
  };

  return (
    <main>
      <div className="text">
        <h2>Händler-Registrierung</h2>
        <p>Bla bla.</p>
      </div>
      <MerchantRegistrationForm
        user={user}
        locked={locked}
        onSubmit={s => createStore(s)}
      />
      {errorMessage && <span className={"error"}>{errorMessage}</span>}
    </main>
  );
};

export default Register;
