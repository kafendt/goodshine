import { Voucher } from "../../model/voucher";
import React from "react";

export interface VoucherVerificationViewProps {
  voucher: Voucher;
}

export const VoucherVerificationView = (
  props: VoucherVerificationViewProps
) => {
  const voucher = props.voucher;
  return <div>Verifiziere: {voucher.id}</div>;
};
