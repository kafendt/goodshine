import * as React from "react";
import { SessionProps } from "../../components/login/PrivateRoute";
import { Redirect, useParams } from "react-router";
import { useVoucherFromToken } from "../../db/voucherStore";
import { VoucherVerificationView } from "./VoucherVerificatioView";

interface VerifyVoucherLocation {
  verificationId: string;
}

const VerifyVoucher: React.FC<SessionProps> = ({ user }) => {
  const { verificationId } = useParams<VerifyVoucherLocation>();
  const idList = verificationId.split("+", 2);
  const [voucher, loading, error] = useVoucherFromToken(idList[0]);
  const shopId = idList[1];
  console.log(idList[0]);
  console.log(voucher);

  return (
    <main>
      <h2>Verification</h2>
      {loading && <span>Loading...</span>}
      {/*TODO redirect to error if shopId doesn't match users shop id (field still required)*/}
      {error && <Redirect to="/error" />}
      {voucher && <VoucherVerificationView voucher={voucher} />}
    </main>
  );
};

export default VerifyVoucher;
