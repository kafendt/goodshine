import { Voucher } from "../../model/voucher";
import React from "react";
import QRCode from "qrcode.react";

export interface VoucherPrintViewProps {
  voucher: Voucher;
}

export const VoucherPrintView = (props: VoucherPrintViewProps) => {
  const voucher = props.voucher;
  return (
    <div>
      <div>Id: {voucher.id}</div>
      <QRCode renderAs="svg" value={voucher.verificationURL} />
      <div>Verification url: {voucher.verificationURL}</div>
      <div>Token: {voucher.token}</div>
      <div>Amount: {voucher.amount}</div>
      <div>Customer id:{voucher.customerId}</div>
      <div>Shop id:{voucher.shopId}</div>
      <div>Creation date: {voucher.creationDate.toString()}</div>
      <div>Log: {voucher.log}</div>
    </div>
  );
};
