import * as React from "react";
import { SessionProps } from "../../components/login/PrivateRoute";
import { Shop } from "../../model/shop";
import Button from "../../components/common/Button";
import { useHistory } from "react-router";

interface ShopProps {
  readonly shops?: { [id: string]: Shop };
}

type HomeProps = ShopProps & SessionProps;

const Home: React.FC<HomeProps> = (props: HomeProps) => {
  const history = useHistory();

  const handleShowDetails = (shopId: string) => {
    history.push(`/customer/shop/${shopId}`);
  };

  return (
    <>
      <div>Welcome, {props.user.displayName}!</div>
      <div>
        {props.shops
          ? Object.values(props.shops).map((shop: Shop) => {
              return (
                <div key={shop.id}>
                  <div>{shop.name}</div>
                  <div>{shop.description}</div>
                  <Button onClick={() => handleShowDetails(shop.id)}>
                    Details anzeigen
                  </Button>
                </div>
              );
            })
          : {}}
      </div>
    </>
  );
};

export default Home;
