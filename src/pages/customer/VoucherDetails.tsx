import * as React from "react";
import { SessionProps } from "../../components/login/PrivateRoute";
import { Redirect, useParams } from "react-router";
import { useVoucher } from "../../db/voucherStore";
import { VoucherPrintView } from "./VoucherPrintView";

interface VoucherDetailsLocation {
  id: string;
}

const VoucherDetails: React.FC<SessionProps> = ({ user }) => {
  const { id } = useParams<VoucherDetailsLocation>();
  const [voucher, loading, error] = useVoucher(id);

  return (
    <main>
      <h2>Voucher</h2>
      {loading && <span>Loading...</span>}
      {error && <Redirect to="/error" />}
      {voucher && <VoucherPrintView voucher={voucher} />}
    </main>
  );
};

export default VoucherDetails;
