import * as React from "react";
import {SessionProps} from "../../components/login/PrivateRoute";
import {Redirect, useParams} from "react-router";
import {useShop} from "../../db/shopStore";
import {Shop} from "../../model/shop";
import {CheckoutForm} from "../../components/customer/CheckoutForm";
import {User} from "../../model/user";

interface ShopDetailsProps {
    user: User;
    shop: Shop;
}

const ShopDetailsPage: React.FC<ShopDetailsProps> = (
    props: ShopDetailsProps
) => {
    const {user, shop} = props;
    console.log("Showing Details");
    return (
        <main>
            <h2>{shop.name}</h2>
            <div>{shop.description}</div>
            <div>Es wäre lieb, wenn du einen Gutschein bei mir kaufst &lt;3</div>
            <CheckoutForm user={user} shopId={shop.id}/>
        </main>
    );
};

interface ShopDetailsLocation {
  id: string;
}

const ShopDetails: React.FC<SessionProps> = ({ user }) => {
  const { id } = useParams<ShopDetailsLocation>();
  const [shop, loading, error] = useShop(id);

  return (
      <main>
          {loading && <span>Loading...</span>}
          {error && <Redirect to="/error"/>}
          {shop && <ShopDetailsPage user={user} shop={shop}/>}
      </main>
  );
};

export default ShopDetails;
