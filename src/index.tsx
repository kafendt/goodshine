import "firebase/firestore";

import React from "react";
import ReactDOM from "react-dom";
import * as firebase from "firebase";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./App";
import * as serviceWorker from "./serviceWorker";

const firebaseConfig = {
  apiKey: "AIzaSyCmkTo-KCRLfRkHnheZdZBqK_7MabBndBk",
  authDomain: "goodshine-ab6c7.firebaseapp.com",
  databaseURL: "https://goodshine-ab6c7.firebaseio.com",
  projectId: "goodshine-ab6c7",
  storageBucket: "goodshine-ab6c7.appspot.com",
  messagingSenderId: "725242408998",
  appId: "1:725242408998:web:1742b9d28a1ddfd97d76e7",
  measurementId: "G-XZ6ZGFWQZR"
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();

ReactDOM.render(
  <>
    <Router basename={"goodshine"}>
      <App />
    </Router>
  </>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
