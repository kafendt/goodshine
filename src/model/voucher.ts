import { DateTime } from "luxon";
import { VoucherDBO, AuditLogEntryDBO } from "../db/dbo/voucher";

export type VoucherState = "created" | "paid" | "used" | "unknown";

export class AuditLogEntry {
  constructor(readonly state: VoucherState, readonly date: DateTime) {}

  serialize(): AuditLogEntryDBO {
    return {
      state: this.state.toString(),
      date: this.date.toISO()
    };
  }

  static fromDBO(dbo: AuditLogEntryDBO) {
    return new AuditLogEntry(
      dbo.state as VoucherState,
      DateTime.fromISO(dbo.date)
    );
  }
}

export class Voucher {
  constructor(
    readonly id: string,
    readonly token: string,
    readonly shopId: string,
    readonly customerId: string,
    readonly amount: number,
    readonly creationDate: DateTime,
    readonly log: AuditLogEntry[]
  ) {}

  get valid(): boolean {
    // TODO: parse log and determine state
    return true;
  }

  get state(): VoucherState {
    if (this.log.length === 0) {
      return "unknown";
    }

    return this.log[this.log.length - 1].state;
  }

  get verificationURL(): string {
    return new URL(
      `/goodshine/merchant/verify/${this.token}+${this.shopId}`,
      window.location.href
    ).href;
  }

  serialize(): VoucherDBO {
    return {
      ...this,
      creationDate: this.creationDate.toISO(),
      log: this.log.map(it => it.serialize())
    };
  }

  static fromDBO(dbo: VoucherDBO) {
    return new Voucher(
      dbo.id,
      dbo.token,
      dbo.shopId,
      dbo.customerId,
      dbo.amount,
      DateTime.fromISO(dbo.creationDate),
      dbo.log.map(entry => AuditLogEntry.fromDBO(entry))
    );
  }
}
