import { UserProfileDBO } from "../db/dbo/user";

export type Role = "customer" | "merchant" | "admin";

export class User {
  constructor(
    readonly id: string,
    readonly displayName: string | undefined,
    readonly email: string | undefined,
    readonly avatarUrl: string | undefined,
    readonly role: Role
  ) {}

  static fromDBO(dao: UserProfileDBO): User {
    return new User(
      dao.id,
      dao.displayName,
      dao.email,
      dao.avatarUrl,
      dao.role
    );
  }
}
