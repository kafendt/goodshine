import firebase from "firebase";
import { ShopDBO, VerificationDocumentDBO } from "../db/dbo/shop";

export type VerificationState = "pending" | "accepted" | "rejected";

export class Shop {
  constructor(
    readonly id: string,
    readonly ownerId: string,
    readonly address: string,
    readonly geolocation: firebase.firestore.GeoPoint | undefined,
    readonly discountPercent: number,
    readonly name: string,
    readonly description: string,
    readonly verificationDocumentId: string,
    readonly state: VerificationState
  ) {}

  static fromDBO(dbo: ShopDBO): Shop {
    return new Shop(
      dbo.id,
      dbo.ownerId,
      dbo.address,
      dbo.geolocation,
      dbo.discountPercent,
      dbo.name,
      dbo.description,
      dbo.verificationDocumentId,
      dbo.state as VerificationState
    );
  }

  serialize(): ShopDBO {
    return {
      ...this
    };
  }

  withState(state: VerificationState): Shop {
    return new Shop(
      this.id,
      this.ownerId,
      this.address,
      this.geolocation,
      this.discountPercent,
      this.name,
      this.description,
      this.verificationDocumentId,
      state
    );
  }
}

export class VerificationDocument {
  constructor(
    readonly id: string,
    readonly ownerId: string,
    readonly blob: firebase.firestore.Blob,
    readonly fileType: string
  ) {}

  static fromDBO(dbo: VerificationDocumentDBO): VerificationDocument {
    return new VerificationDocument(
      dbo.id,
      dbo.ownerId,
      dbo.blob,
      dbo.fileType
    );
  }

  serialize(): VerificationDocumentDBO {
    return { ...this };
  }
}
