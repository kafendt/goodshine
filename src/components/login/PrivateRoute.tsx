import React from "react";
import { Redirect, Route, RouteProps } from "react-router";
import { useUserLoading } from "../../db/userStore";
import { Role, User } from "../../model/user";

// The default value will _never_ be used.
export const LoggedInUserContext = React.createContext<User>(
  new User("", undefined, undefined, undefined, "customer")
);
LoggedInUserContext.displayName = "LoggedInUser";

export interface SessionProps {
  readonly user: User;
}

export default function PrivateRoute(props: RouteProps & { role: Role }) {
  const [user, loading, error] = useUserLoading();

  if (error) {
    console.error("Loading user failed: ", error);
    return <Redirect to={"/error"} />;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!user) {
    return <Redirect to="/login" />;
  }

  if (user.role !== "admin" && user.role !== props.role) {
    return <Redirect to={"/error"} />;
  }

  return (
    <Route {...props}>
      <LoggedInUserContext.Provider value={user}>
        {props.children}
      </LoggedInUserContext.Provider>
    </Route>
  );
}
