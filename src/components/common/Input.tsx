import React, { useState } from "react";

export interface InputProps {
  onBlur: (value: string) => void;
  label: string;
  placeholder: string | undefined;
  disabled?: boolean;
}

export const Input = (props: InputProps) => {
  const [value, setValue] = useState<string>("");
  return (
    <div className="input">
      <label>{props.label}</label>
      <input
        onChange={e => setValue(e.target.value)}
        onBlur={() => props.onBlur(value)}
        placeholder={props.placeholder}
        disabled={props.disabled}
      />
    </div>
  );
};

export default Input;
