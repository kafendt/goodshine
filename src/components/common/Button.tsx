import React from "react";
import clsx from "clsx";

export interface ButtonProps {
  onClick?: () => void;
  children: React.ReactNode;
  style?: "default" | "primary";
  disabled?: boolean;
}

export const Button = (props: ButtonProps) => {
  return (
    <button
      className={clsx("button", props.style ?? "default")}
      disabled={props.disabled}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export default Button;
