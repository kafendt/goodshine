import React, { useState } from "react";

export interface TextBoxProps {
  onBlur: (value: string) => void;
  label: string;
  placeholder: string | undefined;
  maxLength?: number;
  disabled?: boolean;
}

export const TextBox = (props: TextBoxProps) => {
  const [value, setValue] = useState<string>("");
  return (
    <div className="input">
      <label>{props.label}</label>
      <textarea
        onChange={e => setValue(e.target.value)}
        onBlur={() => props.onBlur(value)}
        placeholder={props.placeholder}
        maxLength={props.maxLength}
        disabled={props.disabled}
      />
      {props.maxLength && (
        <span className="max-length">
          {value.length} / {props.maxLength}
        </span>
      )}
    </div>
  );
};

export default TextBox;
