import { SessionProps } from "../login/PrivateRoute";
import React, { ChangeEvent, useState } from "react";
import Input from "../common/Input";
import TextBox from "../common/TextBox";
import firebase from "firebase";
import Button from "../common/Button";

export interface MerchantRegistrationFormProps {
  onSubmit: (s: ShopRegisterFormResult) => void;
  locked: boolean;
}

export interface ShopRegisterFormResult {
  readonly name: string;
  readonly address: string;
  readonly description: string;
  readonly discount: number;
  readonly fileType: string;
  readonly verificationDocument: firebase.firestore.Blob;
}

function validate(
  state: Partial<ShopRegisterFormResult>
): ShopRegisterFormResult {
  const { name, address, description, verificationDocument, fileType } = state;

  if (!name?.length) {
    throw new Error("Bitte geben Sie einen Namen für Ihr Geschäft ein.");
  }

  if (!address?.length) {
    throw new Error("Bitte geben Sie die Addresse Ihres Geschäfts an.");
  }

  if (!description?.length) {
    throw new Error("Bitte beschreiben Sie Ihr Geschäft in einigen Sätzen.");
  }

  if (!verificationDocument || !fileType) {
    throw new Error("Bitte laden Sie ein Dokument hoch.");
  }

  return {
    name,
    address,
    description,
    verificationDocument,
    fileType,
    discount: state.discount ?? 0
  };
}

export const MerchantRegistrationForm = (
  props: MerchantRegistrationFormProps & SessionProps
) => {
  const [state, setState] = useState<Partial<ShopRegisterFormResult>>({});
  const [errorMessage, setErrorMessage] = useState<string | undefined>(
    undefined
  );

  const handleSubmit = () => {
    try {
      const verified = validate(state);
      setErrorMessage(undefined);
      props.onSubmit(verified);
    } catch (e) {
      setErrorMessage(e.message);
    }
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <Input
        onBlur={name => setState({ ...state, name })}
        label={"Name"}
        placeholder={"Bäckerei Franz Muster"}
        disabled={props.locked}
      />
      <TextBox
        onBlur={address => setState({ ...state, address })}
        label={"Adresse"}
        placeholder={"Musterstr. 42\n12345 Musterstadt"}
        disabled={props.locked}
      />
      <TextBox
        onBlur={description => setState({ ...state, description })}
        label={"Beschreibung"}
        placeholder={"Täglich frische Brötchen!"}
        maxLength={512}
        disabled={props.locked}
      />
      <DiscountInput onBlur={discount => setState({ ...state, discount })} />
      <VerificationInput
        onUpload={(verificationDocument, fileType) =>
          setState({ ...state, verificationDocument, fileType })
        }
      />

      {errorMessage && <label className="error">{errorMessage}</label>}
      <Button onClick={() => handleSubmit()} disabled={props.locked}>
        Registrieren
      </Button>
    </form>
  );
};

interface VerificationInputProps {
  onUpload: (blob: firebase.firestore.Blob, fileType: string) => void;
}

const VerificationInput = (props: VerificationInputProps) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target?.files?.item(0);
    if (!file) {
      return;
    }

    const fileReader = new FileReader();
    fileReader.onload = data => {
      try {
        const fileData = data.target?.result as ArrayBuffer;
        if (fileData) {
          const blob = firebase.firestore.Blob.fromUint8Array(
            new Uint8Array(fileData)
          );
          props.onUpload(blob, file.type);
        }
      } catch (e) {
        console.error("Upload failed", e);
      }
    };
    fileReader.readAsArrayBuffer(file);
  };

  return (
    <div className={"input verification"}>
      <label>Verifikationsdokument</label>
      <p className="hint">
        z.B. Gewerbeschein, Personalausweis, Handelsregisterauszug
      </p>
      <input
        type="file"
        accept="image/*,.pdf"
        capture="environment"
        onChange={handleChange}
      />
    </div>
  );
};

interface DiscountInputProps {
  onBlur: (discount: number) => void;
}

const DiscountInput = (props: DiscountInputProps) => {
  const [discount, setDiscount] = useState<number>(0);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const percent = parseInt(e.target.value);
    setDiscount(percent / 100);
  };

  return (
    <div className={"input discount"}>
      <label>Rabatt</label>
      <p className="hint">
        Bieten Sie Ihren Kunden einen zusätzlichen Anreiz, Gutscheine zu kaufen.
      </p>
      <input
        type="number"
        max="100"
        min="0"
        step="1"
        onChange={handleChange}
        onBlur={() => props.onBlur(discount)}
      />
    </div>
  );
};

export default MerchantRegistrationForm;
