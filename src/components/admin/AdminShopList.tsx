import React, { useState } from "react";
import { Shop } from "../../model/shop";
import { Link, useLocation } from "react-router-dom";
import Button from "../common/Button";
import { saveShop } from "../../db/shopStore";
import DocumentViewer from "./DocumentViewer";

export interface AdminShopListProps {
  shops: { [id: string]: Shop };
  filterTerm?: string;
}

function sortByName(a: Shop, b: Shop): number {
  return Intl.Collator().compare(a.name, b.name);
}

export const AdminShopList = (props: AdminShopListProps) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const selectedDocument = new URLSearchParams(useLocation().search).get(
    "document"
  );

  const shops = Object.values(props.shops).filter(it =>
    searchTerm ? it.name?.toLocaleLowerCase().indexOf(searchTerm) >= 0 : true
  );

  const pending = shops.filter(it => it.state === "pending").sort(sortByName);
  const accepted = shops.filter(it => it.state === "accepted").sort(sortByName);
  const rejected = shops.filter(it => it.state === "rejected").sort(sortByName);

  return (
    <>
      <div className="admin-search">
        <SearchInput onChange={term => setSearchTerm(term)} />
      </div>
      {selectedDocument && <DocumentViewer id={selectedDocument} />}
      <div className="admin-shop-list">
        <ListSection shops={pending} label={"Pending"} />
        <ListSection shops={rejected} label={"Rejected"} />
        <ListSection shops={accepted} label={"Accepted"} />
      </div>
    </>
  );
};

interface ListSectionProps {
  shops: Shop[];
  label: string;
}

const ListSection = (props: ListSectionProps) => {
  return (
    <div>
      <h3>{props.label}</h3>

      <table className="list-section">
        <thead className="row header">
          <tr>
            <th>Name</th>
            <th>Adresse</th>
            <th>Beschreibung</th>
            <th>Dokument</th>
            <th>Aktion</th>
          </tr>
        </thead>
        <tbody>
          {props.shops.map(it => (
            <tr className="row" key={it.id}>
              <td>{it.name}</td>
              <td>{it.address}</td>
              <td>{it.description}</td>
              <td>
                <Link to={`?document=${it.verificationDocumentId}`}>
                  Dokument
                </Link>
              </td>
              <td>
                <ShopConfirmationButton shop={it} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

interface ShopConfirmationButtonProps {
  shop: Shop;
}

const ShopConfirmationButton = (props: ShopConfirmationButtonProps) => {
  const state = props.shop.state;
  const [locked, setLocked] = useState<boolean>(false);

  const accept = async () => {
    try {
      setLocked(true);
      await saveShop(props.shop.withState("accepted"));
    } catch (e) {
      console.error("Accepting Shop failed!", e);
      setLocked(false);
    }
  };
  const reject = async () => {
    try {
      setLocked(true);
      await saveShop(props.shop.withState("rejected"));
    } catch (e) {
      console.error("Rejecting Shop failed!", e);
      setLocked(false);
    }
  };

  return (
    <div className="confirmation-button">
      {state !== "accepted" && (
        <Button onClick={accept} disabled={locked}>
          <span role="img" aria-label="accept">
            ✅
          </span>
        </Button>
      )}
      {state !== "rejected" && (
        <Button onClick={reject} disabled={locked}>
          <span role="img" aria-label="reject">
            ❌
          </span>
        </Button>
      )}
    </div>
  );
};

interface SearchInputProps {
  onChange: (term: string) => void;
}

const SearchInput = (props: SearchInputProps) => {
  return (
    <input
      className="input search"
      placeholder={"Suche"}
      onChange={e => props.onChange(e.target.value.toLocaleLowerCase())}
    />
  );
};

export default AdminShopList;
