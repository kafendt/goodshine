import { useDocument } from "../../db/shopStore";
import React from "react";

export interface DocumentViewerProps {
  id: string;
}

export const DocumentViewer = ({ id }: DocumentViewerProps) => {
  const [document, loading, error] = useDocument(id);

  if (error) {
    return <p className={"error"}>Dokument konnte nicht geladen werden.</p>;
  }

  if (loading || !document) {
    return <p>Dokument wird geladen...</p>;
  }

  if (/^image.*/.test(document.fileType)) {
    return (
      <img
        alt="Dokument"
        src={`data:${document.fileType};base64,${document.blob.toBase64()}`}
      />
    );
  }

  return (
    <iframe
      src={`data:${document.fileType};base64,${document.blob.toBase64()}`}
    />
  );
};

export default DocumentViewer;
