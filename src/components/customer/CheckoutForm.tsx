import { useHistory } from "react-router";
import { default as React, useState } from "react";
import { Voucher } from "../../model/voucher";
import uuid from "uuid";
import { DateTime } from "luxon";
import firebase from "firebase";
import Input from "../common/Input";
import Button from "../common/Button";
import { User } from "../../model/user";
import { stripUndefined } from "../../db/util";
import { upsertVoucher } from "../../db/voucherStore";

export interface CheckoutFormProps {
  user: User;
  shopId: string;
}

export interface CheckoutFormResult {
  readonly amount: number;
}

function validate(state: Partial<CheckoutFormResult>): CheckoutFormResult {
  const { amount } = state;

  if (!amount) {
    throw new Error("Bitte geben Sie einen Betrag ein.");
  }
  return {
    amount
  };
}

export const CheckoutForm = (props: CheckoutFormProps) => {
  const { user, shopId } = props;
  const history = useHistory();
  const [formState, setFormState] = useState<Partial<CheckoutFormResult>>({});
  const [errorMessage, setErrorMessage] = useState<string | undefined>(
    undefined
  );

  const handleSubmit = async () => {
    try {
      const verifiedForm = validate(formState);
      await createVoucher(verifiedForm);
    } catch (e) {
      setErrorMessage(e.message);
    }
  };

  const createVoucher = async (formResult: CheckoutFormResult) => {
    const voucher = new Voucher(
      uuid(), //id
      uuid(), //token
      shopId,
      user.id,
      formResult.amount,
      DateTime.utc(),
      []
    );
    await upsertVoucher(voucher);
    history.push(`/customer/voucher/${voucher.id}`);
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <Input
        onBlur={a => setFormState({ ...formState, amount: parseFloat(a) })}
        label={"Betrag"}
        placeholder=""
      />
      {errorMessage && <label className="error">{errorMessage}</label>}
      <Button onClick={() => handleSubmit()}>Kaufen</Button>
    </form>
  );
};
