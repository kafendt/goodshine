import * as functions from "firebase-functions";
import {
  BraintreeGateway,
  Environment,
  MerchantAccountStatus,
  MerchantAddressDetails
} from "braintree";


const gateway = new BraintreeGateway({
  environment: Environment.Sandbox,
  merchantId: functions.config().braintree.merchantid,
  privateKey: functions.config().braintree.privatekey,
  publicKey: functions.config().braintree.publickey
});

// parses an address line to zip code and city (German format)
function parseLine2(line: string): [string, string] {
  const cityRegex = /^.*(\d+)\s?(.*)$/;
  const match = cityRegex.exec(line);

  if (!match || !match.groups) {
    return ["", line];
  }

  return [match.groups[1] ?? "", match.groups[2] ?? ""];
}

function parseAddress(joined: string): MerchantAddressDetails {
  const lines = joined.split("\n");

  if (lines.length === 2) {
    const [postalCode, locality] = parseLine2(lines[1]);
    return {
      streetAddress: lines[0],
      postalCode,
      locality,
      region: "Germany"
    };
  }

  if (lines.length > 2) {
    const [postalCode, locality] = parseLine2(lines[2]);
    return {
      streetAddress: lines[1],
      postalCode,
      locality,
      region: "Germany"
    };
  }

  throw new Error("invalid address");
}

export interface CreateAccountRequest {
  readonly userId: string;
  readonly name: string;
  readonly email: string;
  readonly dateOfBirth: string;
  readonly address: string;

  readonly accountNumber: string;
  readonly routingNumber: string;
}

export interface CreateAccountResponse {
  readonly accountId: string;
  readonly status: MerchantAccountStatus;
}

export const createSubMerchant = functions.https.onRequest(
  async (req, res: functions.Response<CreateAccountResponse>) => {
    try {
      const body = req.body as CreateAccountRequest;
      console.info("Body", body);

      const names = body.name.split(" ");
      const lastName = names.pop() ?? "";

      const result = await gateway.merchantAccount.create({
        id: body.userId,
        individual: {
          firstName: names.join(" "),
          lastName,
          email: body.email,
          address: parseAddress(body.address),
          dateOfBirth: body.dateOfBirth
        },
        funding: {
          destination: "Bank",
          accountNumber: body.accountNumber,
          routingNumber: body.routingNumber
        },
        tosAccepted: true,
        masterMerchantAccountId: functions.config().braintree.merchantid
      });

      if (!result.success) {
        console.dir( result.errors, {depth: null});
        return res.status(400).send();
      }

      return res.status(201).send({
        accountId: result.merchantAccount.id,
        status: result.merchantAccount.status
      });
    } catch (e) {
      console.error("Error:", e);
      return res.status(503).send();
    }
  }
);
